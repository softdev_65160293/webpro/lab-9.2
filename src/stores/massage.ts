import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useMassageStore = defineStore('massage', () => {
  const snackbar = ref(false)
  const text = ref('')
  const showMassage = function (massage: string) {
    text.value = massage
    snackbar.value = true
  }

  return { snackbar, text, showMassage }
})
